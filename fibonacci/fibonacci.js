const readline = require('readline');

function fibonacci(n) {
  console.time('Execution time:');
  if (n < 1) { 
    return 0; 
  }
  let fibNMinus2 = 0;
  let fibNMinus1 = 1;
  let fibN = n;
 
  for (let i = 2; i <= n; i++) {
    fibN = fibNMinus1 + fibNMinus2;
    fibNMinus2 = fibNMinus1;
    fibNMinus1 = fibN;
  }
  console.log(fibN);
  console.timeEnd('Execution time:');
  return fibN;
}

const readLineInterface = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

readLineInterface.question('Enter a number? ', (inputNumber) => {
  fibonacci(inputNumber);
  readLineInterface.close();
});
