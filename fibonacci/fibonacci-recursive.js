const readline = require('readline');

function fibonacci(n) {
  if (n < 1) {
    return 0;
  } else if (n <= 2) {
    return 1;
  } else {
    return fibonacci(n - 1) + fibonacci(n - 2);
  }
}

const readLineInterface = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

readLineInterface.question('Enter a number? ', (inputNumber) => {
  console.time('Execution time:');
  const result = fibonacci(inputNumber);
  console.log(result);
  console.timeEnd('Execution time:');
  readLineInterface.close();
});