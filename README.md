# Algoritmos

## Referências:

* [Interview Cake](https://www.interviewcake.com/sorting-algorithm-cheat-sheet)

## Tabela comparativa:

![Tabela comparativa](tabela-comparativa.png)

## Algoritmos de ordenação:

* Selection sort

![Selection sort](selection-sort/selection-short.png)

```
Lista inicial: [ 29, 72, 98, 13, 87, 66, 52, 51, 36 ]


29 <<<< 13 [ 29, 72, 98, 13, 87, 66, 52, 51, 36 ]
72 <<<< 29 [ 13, 72, 98, 29, 87, 66, 52, 51, 36 ]
98 <<<< 36 [ 13, 29, 98, 72, 87, 66, 52, 51, 36 ]
72 <<<< 51 [ 13, 29, 36, 72, 87, 66, 52, 51, 98 ]
87 <<<< 52 [ 13, 29, 36, 51, 87, 66, 52, 72, 98 ]
87 <<<< 72 [ 13, 29, 36, 51, 52, 66, 87, 72, 98 ]


Lista final: [ 13, 29, 36, 51, 52, 66, 72, 87, 98 ]
```

## Tabela de execução do algoritmo fibonacci:

* Fibonacci

| Número | Tempo      | Resultado |
|--------|------------|-----------|
| 10     | 2.102ms    | 55        |
| 20     | 2.088ms    | 6765      |
| 25     | 2.190ms    | 75025     |
| 30     | 2.088ms    | 832040    |
| 35     | 2.338ms    | 9227465   |
| 40     | 2.068ms    | 102334155 |


* Fibonacci recursivo

| Número | Tempo      | Resultado |
|--------|------------|-----------|
| 10     | 2.157ms    | 55        |
| 20     | 3.720ms    | 6765      |
| 25     | 5.323ms    | 75025     |
| 30     | 18.235ms   | 832040    |
| 35     | 168.841ms  | 9227465   |
| 40     | 1735.009ms | 102334155 |

