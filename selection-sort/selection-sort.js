const Compare = {
  LESS_THAN: -1,
  BIGGER_THAN: 1,
  EQUALS: 0
};

const compareFn = (a, b) => {
  if (a === b) {
    return Compare.EQUALS;
  } else if (a < b) {
    return Compare.LESS_THAN
  } else {
    return Compare.BIGGER_THAN;
  }
}

const swap = (array, a, b) => {
  console.log(`${array[a]} <<<< ${array[b]}`, array);
  [array[a], array[b]] = [array[b], array[a]];
}

const selectionSort = array => {
  console.log('Lista inicial:', array);
  console.log('\n');
  let indexMin;

  for (let i = 0; i < array.length - 1; i++) {
    indexMin = i;

    for (let j = i; j < array.length; j++) {
      if (compareFn(array[indexMin], array[j]) === Compare.BIGGER_THAN) {
        indexMin = j;
      }
    }
    if (i !== indexMin) {
      swap(array, i, indexMin);
    }
  }
  return array;
};

const result = selectionSort([29, 72, 98, 13, 87, 66, 52, 51, 36]);
console.log('\n');
console.log('Lista final:', result);